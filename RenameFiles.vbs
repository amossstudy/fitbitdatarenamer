' Fitbit Data Renamer - A script to rename fitbit data files.
'
' Copyright (c) 2014, Nick Palmius (University of Oxford)
' All rights reserved.
'
' Redistribution and use in source and binary forms, with or without
' modification, are permitted provided that the following conditions are
' met:
'
' 1. Redistributions of source code must retain the above copyright
'    notice, this list of conditions and the following disclaimer.
'
' 2. Redistributions in binary form must reproduce the above copyright
'    notice, this list of conditions and the following disclaimer in the
'    documentation and/or other materials provided with the distribution.
'
' 3. Neither the name of the University of Oxford nor the names of its
'    contributors may be used to endorse or promote products derived
'    from this software without specific prior written permission.
'
' THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
' "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
' LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
' A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
' HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
' SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
' LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
' DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
' THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
' (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
' OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
' Contact: npalmius@googlemail.com
' Originally written by Nick Palmius, 16-Oct-2014

Option Explicit

Dim wsh, fso

Set wsh = CreateObject("WScript.Shell")
Set fso = CreateObject("Scripting.FileSystemObject")

Dim strCurDir, strInDir, strOutDir, strFilename

strCurDir = wsh.CurrentDirectory

If not right(strCurDir, 1) = "\" Then
  strCurDir = strCurDir & "\"
End If

strInDir = strCurDir & "Input\"
strOutDir = strCurDir & "Output\"

Set xl = CreateObject("Excel.Application")

Call xl.ExecuteExcel4Macro("DIRECTORY(""" + strCurDir + """)")

strFilename = xl.GetOpenFilename("Excel Files (*.xls*),*.xls*", 0, "Please choose the Fitbit ID lookup file")

If not strFilename = False Then
  Dim xl, wb, ws, dicIDs, i
  
  Set dicIDs = CreateObject("Scripting.Dictionary")
  
  Set wb = xl.Workbooks.Open(strFilename)
  Set ws = wb.Sheets(1)
  
  for i = 2 to (ws.Range("A2", ws.Range("A2").End(-4121)).Rows.Count + 1)
    call dicIDs.add(ws.Range("A" & CStr(i)).value, ws.Range("B" & CStr(i)).value)
  next
  
  Call wb.close
  Call xl.quit
  
  Dim strMessages, fld, file
  
  strMessages = ""
  
  Set fld = fso.GetFolder(strInDir)
  
  For Each file in fld.Files
    If lcase(right(file.Name, 5)) = ".xlsx" Then
      If dicIDs.Exists(left(file.Name, len(file.Name) - 5)) Then
        If Not fso.FolderExists(strOutDir) Then
          Call fso.CreateFolder(strOutDir)
        End If
        
        Call fso.CopyFile(strInDir & file.Name, strOutDir & dicIDs.Item(left(file.Name, len(file.Name) - 5)) & lcase(right(file.Name, 5)))
      Else
        If len(strMessages) = 0 Then
          strMessages = "The following Fitbit IDs were not found in the lookup file:"
        End If
        strMessages = strMessages & vbCrLf & "  " & left(file.Name, len(file.Name) - 5)
      End If
    End If
  Next
  
  If len(strMessages) = 0 Then
    Call wscript.echo("Done!")
  Else
    Call wscript.echo("Done with warnings!" & vbcrlf & vbcrlf & strMessages)
  End If
Else
  Call wscript.echo("Nothing to do!")
End If
