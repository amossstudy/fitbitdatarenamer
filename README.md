#Fitbit Data Renamer
The Fitbit Data Renamer is script to rename Fitbit data files.

## Contents

The Fitbit Data Renamer consists of two files as follows:

* [`RenameFiles.vbs`](https://bitbucket.org/amossstudy/fitbitdatarenamer/raw/master/RenameFiles.vbs) - This is the VBScript file that performs the renaming procedure.

* [`lookup.xlsx`](https://bitbucket.org/amossstudy/fitbitdatarenamer/src/master/lookup.xlsx) - This is an excel spreadsheet containing a lookup table of Fitbit numbers to Anonymized IDs.


## How to use

1. Place the Fitbit data files in the `Input` folder.

2. Double click on the `RenameFiles.vbs` script.

3. In the dialogue box that opens, select the lookup file to use for renaming.

4. The files in the `Input` folder will be renamed and copied to the `Output` folder.
